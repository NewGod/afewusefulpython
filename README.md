It is a trial fun package created to understand git, gitlab and github with continuous integration and deployment.

![build](https://gitlab.com/newgod/afewusefulpython/badges/master/build.svg) 

[![Documentation Status](https://readthedocs.org/projects/afewusefulpython/badge/?version=latest)](http://afewusefulpython.readthedocs.io/en/latest/?badge=latest)

Thelatest Documentation can be found here: [docs](http://afewusefulpython.readthedocs.io/en/latest/?)