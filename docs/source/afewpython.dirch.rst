afewpython\.dirch package
=========================

Submodules
----------

afewpython\.dirch\.packagify module
-----------------------------------

.. automodule:: afewpython.dirch.packagify
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: afewpython.dirch
    :members:
    :undoc-members:
    :show-inheritance:
