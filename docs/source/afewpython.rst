afewpython package
==================

Subpackages
-----------

.. toctree::

    afewpython.dirch

Submodules
----------

afewpython\.afewpython module
-----------------------------

.. automodule:: afewpython.afewpython
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: afewpython
    :members:
    :undoc-members:
    :show-inheritance:
