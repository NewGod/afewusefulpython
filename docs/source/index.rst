.. afewpython documentation master file, created by
   sphinx-quickstart on Wed Dec 20 02:02:51 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to afewpython's documentation!
======================================
It is a trial fun package created to understand git, gitlab and github with continuous integration and deployment.

Here is the [GitLab Repository](https://gitlab.com/NewGod/afewusefulpython)

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   installation
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
